import React, { Component } from 'react';

class Counter extends Component {

    state = {
        count: 0,
        tags: ["tag1", "tag2", "tag3", "tag4"]
    }

    Styles = {
        fontSize: "10px",
        fontWeight: "Bold"
    }


    // constructor() {
    //     super();
    //     this.handleIncrement = this.handleIncrement.bind(this);
    // }

    renderTags() {
        if (this.state.tags.length === 0) return <p>There are no tags</p>;
        return <ul>{this.state.tags.map(tag => <li key={tag}>{tag}</li>)}</ul>
    }

    handleIncrement = (product) => {
        // console.log('clicked', this);
        this.setState({ count: this.state.count + 1 })
    }

    doHanfdleIncrement = () => {
        this.handleIncrement({ id: 1 });
    }


    render() {
        return (
            <React.Fragment>
                <span className={this.getBadgeClasses()}>{this.state.count}</span>
                <button className="btn btn-default btn-sm" onClick={this.doHanfdleIncrement}>Increment</button>
                {this.renderTags()}
            </React.Fragment>
        );
    }

    getBadgeClasses() {
        let classes = "badge m-2 ";
        classes += (this.state.count === 0) ? "badge-warning" : "badge-primary";
        return classes;
    }
}

export default Counter;